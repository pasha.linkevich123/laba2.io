package senla_java_lab2.constructor;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import senla_java_lab2.entitiy.Port;
import senla_java_lab2.entitiy.Ship;
 @Getter @Setter
public class Context {

    public static Port port;
    public static List<Ship> waitingShips;

}
